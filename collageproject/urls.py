"""collageproject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from collageapp import views
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('admin/', admin.site.urls),
    path("first",views.first,name="first"),
    path("base/",views.base),
    path("contactus/",views.contactus,name="contactus"),

    path("edit/",views.edit,name="editprofile"),
    path("wish/",views.wish,name="wish"),
    path("user_login/",views.user_login,name="login"),
    path("",views.home,name="home"),
    path("Registration/",views.registrationpage,name="Registration"),
    path("All/",views.All,name="all"),
    path("Student/",views.Student,name="student"),
    path("single/",views.single,name="singal"),
    path("update_course/",views.update_course,name="update"),
    path("delete_course/",views.delete_course,name="Delete"),
    path("instructer/",views.instructer,name="instructer"),
    path("reset/",views.reset,name="reset"),
    path("check_user/",views.check_user,name="check_user"),
    path("user_logout/",views.user_logout,name="user_logout"),
    path("addcourse/",views.add_course_view,name="addcourse"),
    path("Mycourse/",views.Mycourse,name="mycourse"),
    path("sendemail/",views.sendemail,name="sendemail"),
    path("forgotpass/",views.forgotpass,name="forgotpass"),
    path("reset_password/",views.reset_password,name="reset_password"),
    path("add_to_cart/",views.add_to_cart,name="add_to_cart"),
    path("get_cart_data/",views.get_cart_data,name="get_cart_data"),
    path("change_quan/",views.change_quan,name="change_quan"),
    path("process_payment/",views.process_payment,name="process_payment"),
    path("payment_done/",views.payment_done,name="payment_done"),
    path("payment_cancelled/",views.payment_cancelled,name="payment_cancelled"),
    path('paypal/', include('paypal.standard.ipn.urls')),
    path("order_history/",views.order_history,name="order_history"),

    

]+static(settings.MEDIA_URL,document_root = settings.MEDIA_ROOT)

