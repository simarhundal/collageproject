from django.contrib import admin

from collageapp.models import (Student,Contact,Category,register_table,Add_Courses,cart,Order)

admin.site.site_header="**PIONEER PYTHON**/**SIMAR HUNDAL**"
class StudentAdmin(admin.ModelAdmin):
     #fields = ["roll_no","name","email"]
    list_display = ["name","roll_no","email","fee","gender","address","is_registered"]
    search_fields = ["roll_no","name"]
    list_filter = ["name","roll_no"]
    list_editable = ["email","address"]

class ContactAdmin(admin.ModelAdmin):

    list_display = ["id","name","email","contact_number","message",]
    search_fields = ["name"]
    list_filter = ["name"]
    list_editable = ["contact_number","message"]

class CategoryAdmin(admin.ModelAdmin):
    list_display = ["id","cat_name","description","added_on"]


# Register your models here.
admin.site.register(Student,StudentAdmin)
admin.site.register(Contact,ContactAdmin)
admin.site.register(register_table)
admin.site.register(Category)
admin.site.register(Add_Courses)
admin.site.register(cart)
admin.site.register(Order)

