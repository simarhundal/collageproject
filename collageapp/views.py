from django.shortcuts import render,get_object_or_404,reverse
from django.http import HttpResponse,HttpResponseRedirect, JsonResponse
from collageapp.models import Contact,register_table ,Add_Courses,Category,cart,Order
from django.contrib.auth.models import User
from django.contrib.auth import login,authenticate,logout
from django.contrib.auth.decorators import login_required
from collageapp.forms import add_courses_form
from django.db.models import Q
from datetime import datetime
from django.core.mail import EmailMessage
import requests
def first(request):
    return render(request,"project aboutus.html")    

def base(request):
    return render(request,"base.html")

def contactus(request):
    if request.method=="POST":
        nm = request.POST["name"]
        em = request.POST["email"]
        con = request.POST["contact"]
        msz = request.POST["message"]

        data = Contact(name=nm,email=em,contact_number=con,message=msz)
        data.save()
        res = "Dear {} Thanks for your Feedback".format(nm)
        return render(request,"Contactus.html",{"status":res})


      
        # res = "Dear {} Thanks for your Feedback".format(nm)
        # return render(request,"Contactus.html",{"status":res})

        return HttpResponse("<h1> Dear {} data save</h1>".format)


       # return HttpResponse("<h1>hello</h1>")
    return render(request,"Contactus.html")


@login_required
def instructer(request):
    context = {}
    check = register_table.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = register_table.objects.get(user__id=request.user.id)
        context["data"] = data
    if "image" in request.FILES:
            img = request.FILES["image"]
            data.profile_pic = img
            data.save()

    return render(request,"Instructerprofile.html",context)

@login_required    
def Student(request):
    context = {}
    check = register_table.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = register_table.objects.get(user__id=request.user.id)
        context["data"] = data
    if "profile_pic" in request.FILES:
            img = request.FILES["profile_pic"]
            data.profile_pic = img
            data.save()
    
    return render(request,"Student profile.html",context)

@login_required    
def user_logout(request):
    logout(request)
    res = HttpResponseRedirect("/")
    res.delete_cookie("user_id")
    res.delete_cookie("date_login")
    return res 

def edit(request):
    context = {}
    check = register_table.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = register_table.objects.get(user__id=request.user.id)
        context["data"]=data
    if request.method=="POST":
        print(request.FILES)
    
        
        fn = request.POST["fname"]
        ln = request.POST["lname"]
        em = request.POST["email"]
        con = request.POST["contact"]
        age = request.POST["age"]
        ct = request.POST["city"]
        occ = request.POST["occ"]
        gen = request.POST["gender"]
        abt = request.POST["about"]

        usr = User.objects.get(id=request.user.id)
        usr.first_name = fn
        usr.last_name = ln
        usr.email = em
        usr.save()
        

        data.contact_number = con
        data.age = age
        data.city = ct
        data.gender = gen
        data.occupation = occ
        data.about = abt
        data.save()
        

        if "profile_pic" in request.FILES:
            img = request.FILES["profile_pic"]
            data.profile_pic = img
            data.save()
        context["status"] = "Changes save successfully"
    return render(request,"Editprofile.html",context)


def add_course_view(request):
    context={}
    check = register_table.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = register_table.objects.get(user__id=request.user.id)
        context["data"] = data
    form = add_courses_form()
    if request.method=="POST": 
        form = add_courses_form(request.POST,request.FILES)
        if form.is_valid():
            data = form.save(commit=False) 
            login_user = User.objects.get(username=request.user.username)
            data.instructor = login_user
            data.save()
            context["status"] = "{} addedd successfully".format(data.course_name)


    context["form"] = form
    return render(request,"Addcourse.html",context)

def Mycourse(request):
    context = {}
    check = register_table.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = register_table.objects.get(user__id=request.user.id)
        context["data"] = data
    all = Add_Courses.objects.filter(instructor__id=request.user.id).order_by("-id")
    context["courses"] = all
    return render(request,"My Course.html",context)  


def single(request):
    context = {}
    id = request.GET["cid"]
    obj = Add_Courses.objects.get(id=id)
    context["course"] = obj
    return render(request,"singlecourse.html",context)    



def update_course(request):
    context ={}
    cats = Category.objects.all().order_by("cat_name")
    context["cats"] = cats
    id = request.GET["cid"]
    Course = get_object_or_404(Add_Courses,id=id)
    context["course"] = Course
    if request.method=="POST":
        cn = request.POST["cname"]
        ct_id = request.POST["ccat"]
        pr = request.POST["cp"]
        sp = request.POST["sp"]
        des = request.POST["des"]

        cat_obj = Category.objects.get(id=ct_id)


        Course.course_name =cn
        Course.course_category =cat_obj
        Course.course_price = pr
        Course.sale_price =sp
        Course.course_details =des
        if "cimg" in request.FILES:
            img = request.FILES["cimg"]
            Course.course_image = img
        Course.save()
        context["status"] = "Changes Save Successfully"
        context["id"] = id
    return render(request,"Update_course.html",context)     

    
def delete_course(request):
    context = {}
    if "cid" in request.GET:
        cid = request.GET["cid"]
        Course = get_object_or_404(Add_Courses,id=cid)
        context["course"] = Course

        if "action" in request.GET:
            Course.delete()
            context["status"] = str(Course.course_name)+"Delete Successfully!!!"
    
    return render(request,"Delete_course.html",context)


def All(request):
    context = {}
    All = Add_Courses.objects.all().order_by("course_name") 
    context["courses"] = All 
    if "qry" in request.GET:
        q = request.GET["qry"]
        # p = request.GET["qry"]
        Course = Add_Courses.objects.filter(Q(course_name__icontains=q)|Q(course_category__cat_name__contains=q))
        # Course = Add_Courses.objects.filter(Q(course_name__icontains=q)|Q(sale_price<))
        context["courses"] = Course
        context["abcd"]="search"
    if "cat" in request.GET:
        pid = request.GET["cat"]
        Course =  Add_Courses.objects.filter(course_category__id=pid)
        context["course"] = Course
        context["abcd"]="search"    
    return render(request,"All course.html",context)

def wish(request):
    return render(request,"wish.html")    
def user_login(request):
    if request.method=="POST":
        un = request.POST["username"]
        pwd = request.POST["password"]
        

        user = authenticate(username=un,password=pwd)
        if user :
            login(request,user)
            if user.is_superuser:
                return HttpResponseRedirect("/admin")
            elif user.is_staff:
                res = HttpResponseRedirect("/instructer")
                if "remeberme" in request.POST:
                    res.set_cookie("user_id",user.id)
                    res.set_cookie("date_login",datetime.now())
                return res    
                
            elif user.is_active: 
                return HttpResponseRedirect("/Student")
        else:
            return render(request,"homenew.html",{"status":"Invalid Username or Password"}) 
        return HttpResponse("called") 
            
    return render(request,"login.html")    

    
    
    
def home(request):
    # if "user_id"in request.COOKIES:
    #     uid = request.COOKIES["user_id"]
    #     usr = get_object_or_404(User,id=uid)
    #     login(request,usr)
    #     if usr.is_superuser:
    #         return HttpResponseRedirect("/admin")

    #     if usr.is_active:
    #         return HttpResponseRedirect("/instructer")
    cats = Category.objects.all()
    return render(request,"homenew.html",{"category":cats}) 



# def Registration(request):
#     if "user_id"in request.COOKIES:
#         uid = request.COOKIES["user_id"]
#         usr = get_object_or_404(User,id=uid)
#         login(request,usr)
#         if usr.is_superuser:
#             return HttpResponseRedirect("/admin")

#         if usr.is_active:
#             return HttpResponseRedirect("/student")
#     if request.method=="POST":
#         fname = request.POST["first"]
#         last = request.POST["last"]
#         un = request.POST["myuser"]
#         pwd = request.POST["password1"]
#         cpwd = request.POST["password2"]
#         em = request.POST["email"]
#         con = request.POST["contact"]
        
#         # tp = request.POST["utype"]
#         # print(request.POST)
#         usr = User.objects.create_user(un,em,pwd)
#         usr.username = un 
#         usr.first_name = fname
#         usr.last_name = last
#         # if tp=="teach":
#         #     usr.is_staff=True
#         usr.save()

#         reg = register_table(user=usr,contact_number=con)
#         reg.save()

#         return render(request,"Registration.html",{"status":" register successfully".format(fname)})  
        
#     return render(request,"Registration.html")

def registrationpage(request):
    context={}
    if "user_id" in request.COOKIES:
        uid = request.COOKIES["user_id"]
        usr = get_object_or_404(User,id=uid)
        login(request,usr)
        if usr.is_superuser:
            return  HttpResponseRedirect("/admin")

        if usr.is_staff:
            return  HttpResponseRedirect("/instructor")
        if usr.is_active:
            return  HttpResponseRedirect("/Student")   
    if request.method=="POST":
        fname = request.POST["first"]
        last = request.POST["last"]
        usernm  = request.POST["myuser"]
        con = request.POST["contact"]
        pwd = request.POST["password1"] 
        em = request.POST["email"]
        utype = request.POST["utype"]
        usr = User.objects.create_user(usernm,em,pwd)
        usr.username = usernm 
        usr.first_name = fname
        usr.last_name = last
        usr.save()
        if utype =="fmle":
            usr.is_staff=True
            usr.save()
        reg = register_table(user=usr,contact_number=con)
        reg.save()
        context={"status":"Dear {} Thanks for registration".format(fname)}

    
    return render(request,"registration2.html",context)


def sendemail(request):
    context = {}
    check = register_table.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = register_table.objects.get(user__id=request.user.id)
        context["data"]=data


    if request.method=="POST":
        
        rec = request.POST["to"].split(",")
        sub = request.POST["sub"]
        msz = request.POST["msz"]
        try:    
            em = EmailMessage(sub,msz,to=rec)
            
            em.send()
            context["status"] = "Email Sent"
            context["cls"] = "alert-success"
        except:
            context["status"] = " Could not Send ,Please Check internet connnection /Email address"
            context["cls"] = "alert-danger"

    return render(request,"sendemail.html",context )




    if request.method=="POST":
        fname = request.POST["first"]
        lname = request.POST["last"]
        uname = request.POST["myuser"]
        em = request.POST["email"]
        con = request.POST["contact"]
        pwd = request.POST["password1"]
        cpwd = request.POST["password2"]
        tp = request.POST["utype"]

        usr = User.objects.create_user(uname,em,pwd)
        usr.first_name = fname
        usr.last_name = lname
        if tp=="teacher":
            usr.is_staff = True
        usr.save()

        reg = register_table(user=usr,contact_number=con)
        reg.save()
        return render(request,"Registration.html",{"status":"Mr/Ms.{} Your Account Creat Successfully"})

       
    return render(request,"Registration.html")

def check_user(request):
    if request.method=="GET":
        un = request.GET["usern"]
        check = User.objects.filter(username=un)
        if len(check) == 1:
            return HttpResponse("Exists")
        else:
            return HttpResponse("Not Exists")










def reset(request):
    context={}
    check = register_table.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = register_table.objects.get(user__id=request.user.id)
        context["data"] = data

    if request.method=="POST":
        current = request.POST["cpwd"]
        new_pas = request.POST["npwd"]
        

        user = User.objects.get(id=request.user.id)
        un = user.username
        check = user.check_password(current)
        if check==True:
            user.set_password(new_pas)
            user.save()
            context["msz"] = "Password Changed Successfully!!!"
            context["col"] = "alert-success"
            user = User.objects.get(username=un)
            login(request,user)
        else:
            context["msz"] = "Incorrect current password"
            context["col"] = "alert-danger"

        



    return render(request,"Reset.html",context)




def forgotpass(request):
    context = {}
    if request.method=="POST":
        un = request.POST["username"]
        pwd = request.POST["npass"]

        user = get_object_or_404(User,username=un)
        user.set_password(pwd)
        user.save()

        login(request,user)
        if user.is_superuser:
            return HttpResponseRedirect("/admin")
        else:
            return HttpResponseRedirect("/instructer")    
        # context["status"] = "Password Change Successfully!!!"
    return render(request,"Forgot Password.html",context)

import random

def reset_password(request):
    un = request.GET["username"]
    try:
        user = get_object_or_404(User,username=un) 
        otp = random.randint(1000,9999)
        msz = "Dear {} \n{} is your OTP \nDo not share with anyone \nThanku \nPIONEER PYTHON".format(user.username,otp)
        try:
            email = EmailMessage("Account Verification",msz,to=[user.email])
            email.send()
            return JsonResponse({"status":"sent","email":user.email,"rotp":otp})

        except:
            return JsonResponse({"status":"Error","email":user.email})
      
        
    except:
        return JsonResponse({"status":"failed"})

def add_to_cart(request):
    context={}
    items = cart.objects.filter(user__id=request.user.id,status=False)
    context["items"] = items
    if request.user.is_authenticated:
        if request.method=="POST":
            cid = request.POST["cid"]
            
            qty = request.POST["qty"]
            is_exist = cart.objects.filter(course__id=cid,user__id=request.user.id,status=False)
            if len(is_exist)>0:
                context["msz"] = "Item Already Exists in Your Cart"
                context["cls"] = "alert alert-warning"
            else:   
                course =get_object_or_404(Add_Courses,id=cid)
                usr = get_object_or_404(User,id=request.user.id)
                c = cart(user=usr,course=course,quantity=qty)
                c.save()
                context["msz"] = "{} Added in Your Cart".format(course.course_name)
                context["cls"] = "alert alert-success"

    else:
        context["status"] = "Please Login First to View Your Cart"
    return render(request,"cart.html",context)


def get_cart_data(request):
    items = cart.objects.filter(user__id=request.user.id, status=False)
    sale,total,quantity =0,0,0
    for i in items:
        sale += float(i.course.sale_price)*i.quantity
        total += float(i.course.course_price)*i.quantity
        quantity += int(i.quantity)

    res = {
        "total":total,"offer":sale,"quan":quantity,
        }    
    return JsonResponse(res)


def change_quan(request):
    if "quantity" in request.GET:
        pid = request.GET["pid"]
        qty = request.GET["quantity"]
        cart_obj = get_object_or_404(cart,id=pid)
        cart_obj.quantity = qty
        cart_obj.save()
        return HttpResponse(cart_obj.quantity)
    if "delete_cart" in request.GET:
        id = request.GET["delete_cart"]
        cart_obj = get_object_or_404(cart,id=id)
        cart_obj.delete()
        return HttpResponse(1)

from paypal.standard.forms import PayPalPaymentsForm
from django.conf import settings

def process_payment(request):
    items = cart.objects.filter(user_id__id=request.user.id,status=False)
    course=""
    amt=0
    inv = "INV1001-"
    cart_ids = ""
    c_ids =""

    for j in items:
        course += str(j.course.course_name)+"\n"
        c_ids += str(j.course.id)+","
        amt += float(j.course.sale_price)
        inv += str(j.id)
        cart_ids += str(j.id)+","
    paypal_dict = {
        'business': settings.PAYPAL_RECEIVER_EMAIL,
        'amount':str(amt),                   
        'item_name': course,
        'invoice': inv,
        'notify_url': 'http://{}{}'.format("127.0.0.1:8000",
                                           reverse('paypal-ipn')),
        'return_url': 'http://{}{}'.format("127.0.0.1:8000",
                                           reverse('payment_done')),
        'cancel_return': 'http://{}{}'.format("127.0.0.1:8000",
                                              reverse('payment_cancelled')),
                                                    
    }
    usr = User.objects.get(username=request.user.username)
    ord = Order(Student_id=usr,cart_ids=cart_ids,course_ids=c_ids)
    ord.save()
    ord.invoice_id = str(ord.id)+inv
    ord.save()
    request.session["order_id"] = ord.id
 
    form = PayPalPaymentsForm(initial=paypal_dict)
    return render(request, 'process_payment.html', {'form': form})
def payment_done(request):
    if "order_id" in request.session:
        order_id = request.session["order_id"]
        ord_obj = get_object_or_404(Order,id=order_id)
        ord_obj.status=True
        ord_obj.save()
        

        for i in ord_obj.cart_ids.split(",")[:-1]:
            cart_object = cart.objects.get(id=i)
            cart_object.status=True
            cart_object.save()
    return render(request,"Payment_Success.html")    
def payment_cancelled(request):
    return render(request,"Payment_failed.html")        


def order_history(request):
    context = {}
    check = register_table.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = register_table.objects.get(user__id=request.user.id)
        context["data"]=data
    all_orders = []    
    orders = Order.objects.filter(Student_id__id=request.user.id).order_by("-id")
    for order in orders:
        course = []
        for id in order.course_ids.split(",")[:-1]:
            cour = get_object_or_404(Add_Courses,id=id)
            course.append(cour)
        ord = {
            "order_id":order.id,
            "course":course,
            "invoice":order.invoice_id,
            "status":order.status,
            "date":order.processed_on,
        }
        all_orders.append(ord)
    context["order_history"] = all_orders     


    return render(request,"order_history.html",context)   