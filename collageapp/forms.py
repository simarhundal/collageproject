from django import forms
from collageapp.models import Add_Courses

class add_courses_form(forms.ModelForm):
    class Meta:
        model = Add_Courses
        exclude = ["instructor","instructor_name"]